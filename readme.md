# MIT Course 6.006 - Introduction to Algorithms (Fall 2011)

Lecture videos, exams, assignments and other materials are downloaded from [MIT OpenCourseWare] (http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/index.htm).

## Prerequisites

A firm grasp of Python and a solid background in discrete mathematics are necessary prerequisites to this course.

 - 6.01 - [Introduction to EECS](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-01sc-introduction-to-electrical-engineering-and-computer-science-i-spring-2011/)
 - 6.042J - [Mathematics for Computer Science](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-042j-mathematics-for-computer-science-fall-2010/)


## Course Description

This course provides an introduction to mathematical modeling of computational problems. It covers the 
common algorithms, algorithmic paradigms, and data structures used to solve these problems. The course
emphasizes the relationship between algorithms and programming, and introduces basic performance measures
and analysis techniques for these problems.


## Assignments

We will assign seven problem sets during the course of the semester. Each problem set will consist of
a programming assignment, to be completed in Python, and a theory assignment.


## Textbook

Cormen, Thomas, Charles Leiserson: [Introduction to Algorithms](introduction to algorithms.pdf), MIT Press, 2009. ISBN: 9780262033848.
