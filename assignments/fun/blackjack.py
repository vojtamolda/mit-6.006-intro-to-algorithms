# /usr/bin/env python
# coding=utf-8
#
# Perfect-Information Blackjack (Recitation 20)
#
# We want to play Blackjack against a dealer (with no other players). Suppose,
# in addition, that we have x-ray vision that allows us to see the entire
# deck (c0, c1, ..., cn−1). As in a casino, the dealer will use a fixed
# strategy that we know ahead of time (stand-on-17), and that we are allowed
# to make $1 bets (so with each round, we can either win $1, lose $1, or tie
# and make no profits and no losses). How do we maximize our winnings in this
# game? When should we hit or stand?

import unittest
import random


c = [value for symbol in ['♠', '♥', '♦', '♣']
           for value in [2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11]]
random.shuffle(c)
parent = {}
n = len(c)


def BJ(i):
    options = [0]  # we can always leave the table
    if n - 1 < 4:
        return 0  # since there are not enough cards
    for p in range(2, n - i - 2):  # number of cards taken
        # player’s cards by deal order (player, then dealer, then player)
        player = c[i] + c[i + 2] + sum(c[i + 4: i + p + 2])
        if player > 21:  # bust
            options.append(-1 + BJ(i + p + 2))
            parent[i] = i + p + 2
            break
        for d in range(2, n - i - p):
            dealer = c[i + 1] + c[i + 3] + sum(c[i + p + 2: i + p + d])
            if dealer >= 17:
                break
            if dealer > 21:
                dealer = 0  # bust
            options.append(cmp(player, dealer) + BJ(i + p + d))
            parent[i] = i + p + d
    return max(options)


class BJTest(unittest.TestCase):
    def test_BJ(self):
        print("Winnigs: ${}".format(BJ(0)))
        print("Rounds: ")
        i = 0
        while parent[i] in parent:
            print(parent[i])
            i = parent[i]
