# /usr/bin/env python
# coding=utf-8
#
# Longest Increasing Subsequence (Recitation 20)
#
# Find a longest sub-sequence of a given sequence in which the subsequence
# elements are in sorted order. Subsequence can skip elements in the original
# sequence.
#

import unittest


def longest_increasing_subsequence(sequence):
    longest_subs = [[num] for num in sequence]

    def relax_longest_subs(start):
        longest_sub = []
        for candidate in range(start, len(sequence)):
            if (sequence[start] < sequence[candidate] and
                    len(longest_sub) < len(longest_subs[candidate])):
                longest_sub = longest_subs[candidate]
        longest_subs[start] = [sequence[start]] + list(longest_sub)

    for start in reversed(range(len(sequence))):
        relax_longest_subs(start)

    longest_subs.sort(key=len, reverse=True)
    return longest_subs[0]


class SubsequenceTest(unittest.TestCase):
    def test_longest_increasing_subsequence(self):
        sequence = [3, 5, 2, 1]
        longest = longest_increasing_subsequence(sequence)
        self.assertListEqual(longest, [3, 5])

        sequence = [0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15]
        longest = longest_increasing_subsequence(sequence)
        self.assertTrue(longest == [0, 2, 6, 9, 11, 15] or
                        longest == [0, 2, 6, 9, 13, 15] or
                        longest == [0, 4, 6, 9, 11, 15] or
                        longest == [0, 4, 6, 9, 13, 15], longest)
