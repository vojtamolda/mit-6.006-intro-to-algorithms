#!/usr/bin/env python

import unittest
import random
import glob
import sys
import re

from circuit2 import *


class Circuit2Test(unittest.TestCase):
    def setUp(self):
        dir = os.path.dirname(__file__)
        # Obtains a list of cross-platform test file paths.
        self._in_files = glob.glob(os.path.join(dir, 'tests', '*.in'))
        self._in_files.sort()

    def _cmp_lists(self, file, result_set):
        crossings = {}
        for crossing in result_set.crossings:
            key = tuple(crossing)
            if key in crossings:
                # Same crossing reported twice.
                return False
            crossings[key] = True

        crossing_count = len(crossings)
        while True:
            line = file.readline()
            if len(line) == 0:
                return crossing_count == 0
            key = tuple(line.split())
            if key in crossings:
                crossing_count -= 1
            else:
                return False

    def _cmp_counts(self, file, result):
        return int(file.readline()) == result

    def testCorrectness(self):
        print 'Testing correctness:'
        for in_filename in self._in_files:
            list_test = in_filename.find("list_") >= 0
            print 'Testing {0} ......'.format(os.path.basename(in_filename)),
            sys.stdout.flush()
            with open(in_filename) as in_file:
                layer = WireLayer.from_file(in_file)
                verifier = CrossVerifier(layer)
                if list_test:
                    result = verifier.wire_crossings()
                else:
                    result = verifier.count_crossings()

                gold_filename = re.sub('\.in$', '.gold', in_filename)
                with open(gold_filename) as gold_file:
                    if list_test:
                        same = self._cmp_lists(gold_file, result)
                    else:
                        same = self._cmp_counts(gold_file, result)

                    if same:
                        print 'OK'
                    else:
                        print 'Failed'
                    self.assertTrue(same)


class BinaryRangeIndexTest(unittest.TestCase):
    def testSearch(self):
        idx = BinaryRangeIndex()
        keys = [i for i in range(9)]
        for key in keys:
            idx.add(key)
        for key in keys:
            index = idx._binary_search(idx.data, key)
            baseline = keys.index(key)
            self.assertEqual(baseline, index)
        index_overflow = idx._binary_search(idx.data, max(keys) + 1)
        baseline_overflow = len(keys)
        self.assertEqual(baseline_overflow, index_overflow)
        index_underflow = idx._binary_search(idx.data, min(keys) - 1)
        baseline_underflow = 0
        self.assertEqual(baseline_underflow, index_underflow)

        idx = BinaryRangeIndex()
        keys = [0, 2, 55, 512, 1221, 15886, 2054877]
        for key in keys:
            idx.add(key)
        for key in keys:
            index = idx._binary_search(idx.data, key)
            baseline = keys.index(key)
            self.assertEqual(baseline, index)
        index_overflow = idx._binary_search(idx.data, max(keys) + 1)
        baseline_overflow = len(keys)
        self.assertEqual(baseline_overflow, index_overflow)
        index_underflow = idx._binary_search(idx.data, min(keys) - 1)
        baseline_underflow = 0
        self.assertEqual(baseline_underflow, index_underflow)

        idx = BinaryRangeIndex()
        index_empty = idx._binary_search(idx.data, 1)
        baseline_empty = 0
        self.assertEqual(baseline_empty, index_empty)

    def testAddRemove(self):
        idx = BinaryRangeIndex()
        chk = ArrayRangeIndex()
        keys = [1231, 2, 409, 124, 1412351, 125, 5, 596, 1221]
        for key in keys:
            idx.add(key)
            chk.add(key)
            self.assertListEqual(sorted(chk.data), idx.data)
        random.seed(123)

        while len(keys) > 0:
            randkey = random.choice(keys)
            keys.remove(randkey)
            idx.remove(randkey)
            chk.remove(randkey)
            self.assertListEqual(sorted(chk.data), idx.data)

    def testCount(self):
        idx = BinaryRangeIndex()
        chk = ArrayRangeIndex()
        keys = [1, 2, 5, 15, 22, 45, 52, 61, 93]
        for key in keys:
            idx.add(key)
            chk.add(key)

        count_min_to_max = idx.count(min(keys), max(keys))
        check_min_to_max = chk.count(min(keys), max(keys))
        self.assertEqual(check_min_to_max, count_min_to_max)

        count_minn_to_maxx = idx.count(min(keys) - 1, max(keys) + 1)
        check_minn_to_maxx = chk.count(min(keys) - 1, max(keys) + 1)
        self.assertEqual(check_minn_to_maxx, count_minn_to_maxx)

        count_2_to_22 = idx.count(2, 22)
        check_2_to_22 = chk.count(2, 22)
        self.assertEqual(check_2_to_22, count_2_to_22)

        count_1_to_15 = idx.count(1, 15)
        check_1_to_15 = chk.count(1, 15)
        self.assertEqual(check_1_to_15, count_1_to_15)

        count_2_to_93 = idx.count(2, 93)
        check_2_to_93 = chk.count(2, 93)
        self.assertEqual(check_2_to_93, count_2_to_93)

        count_0_to_45 = idx.count(0, 45)
        check_0_to_45 = chk.count(0, 45)
        self.assertEqual(check_0_to_45, count_0_to_45)

        count_52_to_100 = idx.count(52, 100)
        check_52_to_100 = chk.count(52, 100)
        self.assertEqual(check_52_to_100, count_52_to_100)

    def testList(self):
        idx = BinaryRangeIndex()
        chk = ArrayRangeIndex()
        keys = [1, 2, 5, 15, 22, 45, 52, 61, 93]
        for key in keys:
            idx.add(key)
            chk.add(key)

        list_min_to_max = idx.list(min(keys), max(keys))
        check_min_to_max = idx.list(min(keys), max(keys))
        self.assertListEqual(check_min_to_max, list_min_to_max)

        list_minn_to_maxx = idx.list(min(keys) - 1, max(keys) + 1)
        check_minn_to_maxx = chk.list(min(keys) - 1, max(keys) + 1)
        self.assertListEqual(check_minn_to_maxx, list_minn_to_maxx)

        list_2_to_22 = idx.list(2, 22)
        check_2_to_22 = chk.list(2, 22)
        self.assertEqual(check_2_to_22, list_2_to_22)

        list_0_to_45 = idx.list(0, 45)
        check_0_to_45 = chk.list(0, 45)
        self.assertEqual(check_0_to_45, list_0_to_45)

        list_52_to_100 = idx.list(52, 100)
        check_52_to_100 = chk.list(52, 100)
        self.assertEqual(check_52_to_100, list_52_to_100)


if __name__ == '__main__':
    unittest.main()
