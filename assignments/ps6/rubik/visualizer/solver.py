import rubik
from collections import deque

class FrontiersCollided(Exception):
    pass

def shortest_path(start, end):
    """
    Using 2-way BFS, finds the shortest path from start_position to
    end_position. Returns a list of moves.

    You can use the rubik.quarter_twists move set.
    Each move can be applied using rubik.perm_apply
    """
    def reconstruct_moves(fwd_parents, rwd_parents, intersecting_cube_state):
        moves = []
        fwd_state = intersecting_cube_state
        while fwd_parents[fwd_state] is not None:
            fwd_moving_twist = fwd_parents[fwd_state]
            rwd_moving_twist = rubik.perm_inverse(fwd_moving_twist)
            moves.append(fwd_moving_twist)
            fwd_state = rubik.perm_apply(rwd_moving_twist, fwd_state)
        moves.reverse()
        rwd_state = intersecting_cube_state
        while rwd_parents[rwd_state] is not None:
            rwd_moving_twist = rwd_parents[rwd_state]
            fwd_moving_twist = rubik.perm_inverse(rwd_moving_twist)
            moves.append(fwd_moving_twist)
            rwd_state = rubik.perm_apply(fwd_moving_twist, rwd_state)
        return moves

    fwd_parents = {start: None}
    fwd_frontier = deque([start])
    fwd_moving_twists = [perm for perm in rubik.quarter_twists]

    rwd_parents = {end: None}
    rwd_frontier = deque([end])
    rwd_moving_twists = [rubik.perm_inverse(perm) for perm in rubik.quarter_twists]

    level = 0
    while level <= 7:
        level += 1

        next_frontier = deque()
        while len(fwd_frontier) > 0:
            fwd_state = fwd_frontier.popleft()
            for move in fwd_moving_twists:
                cube_state = rubik.perm_apply(move, fwd_state)
                if cube_state not in fwd_parents:
                    fwd_parents[cube_state] = move
                    next_frontier.append(cube_state)
            if fwd_state in rwd_parents:
                return reconstruct_moves(fwd_parents, rwd_parents, fwd_state)
        fwd_frontier = next_frontier

        next_frontier = deque()
        while len(rwd_frontier) > 0:
            rwd_state = rwd_frontier.popleft()
            for move in rwd_moving_twists:
                cube_state = rubik.perm_apply(move, rwd_state)
                if cube_state not in rwd_parents:
                    rwd_parents[cube_state] = move
                    next_frontier.append(cube_state)
            if rwd_state in fwd_parents:
                return reconstruct_moves(fwd_parents, rwd_parents, rwd_state)
        rwd_frontier = next_frontier

    return None
