import imagematrix


class ResizeableImage(imagematrix.ImageMatrix):
    def best_seam(self):
        dp = [[0 for j in range(self.height)] for i in range(self.width)]
        p = [[None for j in range(self.height)] for i in range(self.width)]

        for i in range(self.width):
            dp[i][0] = self.energy(0, j)
            p[i][0] = None

        for j in range(1, self.height):
            for i in range(self.width):
                min, argmin = dp[i + 0][j - 1], (i + 0, j - 1)
                if i > 0 and dp[i - 1][j - 1] < min:
                    min, argmin = dp[i - 1][j - 1], (i - 1, j - 1)
                if i < self.width - 1 and dp[i + 1][j - 1] < min:
                    min, argmin = dp[i + 1][j - 1], (i + 1, j - 1)
                dp[i][j] = min + self.energy(i, j)
                p[i][j] = argmin

        seam_energy, seam_start = float("inf"), None
        for i in range(self.width):
            if dp[i][self.height - 1] < seam_energy:
                seam_energy, seam_start = dp[i][self.height - 1], (i, self.height - 1)

        retval = []
        next_pixel = seam_start
        while next_pixel is not None:
            retval.append(next_pixel)
            next_pixel = p[next_pixel[0]][next_pixel[1]]

        return retval

    def remove_best_seam(self):
        self.remove_seam(self.best_seam())
