#!/usr/bin/env python2.7

from dnaseqlib import *
from collections import deque

### Utility classes ###

# Maps integer keys to a set of arbitrary values.
class Multidict:
    # Initializes a new multi-value dictionary, and adds any key-value
    # 2-tuples in the iterable sequence pairs to the data structure.
    def __init__(self, pairs=[]):
        self.dict = dict()
        for k, v in pairs:
            self.put(k, v)

    # Associates the value v with the key k.
    def put(self, k, v):
        try:
            self.dict[k].append(v)
        except KeyError:
            self.dict[k] = [v]

    # Gets any values that have been associated with the key k; or, if
    # none have been, returns an empty sequence.
    def get(self, k):
        try:
            return self.dict[k]
        except KeyError:
            return []


# Given a sequence of nucleotides, return all k-length subsequences
# and their hashes.  (What else do you need to know about each
# subsequence?)
def subsequenceHashes(seq, k):
    rhash = RollingHash(["0" for _ in range(k)])
    subsequence = deque(maxlen=k)
    i = 0
    for nucleotide in seq:
        if i < k:
            rhash.slide("0", nucleotide)
            subsequence.append(nucleotide)
        else:
            rhash.slide(subsequence.popleft(), nucleotide)
            subsequence.append(nucleotide)
            yield rhash.current_hash(), (i - k + 1, "".join(subsequence))
        i += 1
    raise StopIteration


# Similar to subsequenceHashes(), but returns one k-length subsequence
# every m nucleotides.  (This will be useful when you try to use two
# whole data files.)
def intervalSubsequenceHashes(seq, k, m):
    subsequence = deque(maxlen=k)
    i = 0
    for nucleotide in seq:
        if i < k:
            subsequence.append(nucleotide)
        else:
            subsequence.append(nucleotide)
            if (i - k) % m == 0:
                rhash = RollingHash([nuc for nuc in subsequence])
                yield rhash.current_hash(), (i - k + 1, "".join(subsequence))
        i += 1
    raise StopIteration


# Searches for commonalities between sequences a and b by comparing
# subsequences of length k.  The sequences a and b should be iterators
# that return nucleotides.  The table is built by computing one hash
# every m nucleotides (for m >= k).
def getExactSubmatches(a, b, k, m):
    asubstring_hashes = Multidict()
    for ahash, (aindex, asubsequence) in intervalSubsequenceHashes(a, k, m):
        asubstring_hashes.put(ahash, (aindex, asubsequence))

    for bhash, (bindex, bsubsequence) in subsequenceHashes(b, k):
        for (aindex, asubsequence) in asubstring_hashes.get(bhash):
            if asubsequence == bsubsequence:
                yield (aindex, bindex)


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('Usage: {0} [file_a.fa] [file_b.fa] [output.png]'.format(sys.argv[0]))
        sys.exit(1)

    # The arguments are, in order: 1) Your getExactSubmatches
    # function, 2) the filename to which the image should be written,
    # 3) a tuple giving the width and height of the image, 4) the
    # filename of sequence A, 5) the filename of sequence B, 6) k, the
    # subsequence size, and 7) m, the sampling interval for sequence
    # A.
    compareSequences(getExactSubmatches, sys.argv[3], (500, 500), sys.argv[1], sys.argv[2], 8, 100)
